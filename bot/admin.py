from django.contrib import admin

from bot.models import Profile, Tovar, Dolg,Message


# Register your models here.
@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('iduser', 'username', 'name')


@admin.register(Tovar)
class TovarAdmin(admin.ModelAdmin):
    list_display = ('id', 'category', 'name', 'price', 'inprice')


@admin.register(Dolg)
class DolgAdmin(admin.ModelAdmin):
    list_display = ('user', 'tovar', 'amount', 'created_at')


@admin.register(Message)
class DolgAdmin(admin.ModelAdmin):
    list_display = ('user', 'created_at')
