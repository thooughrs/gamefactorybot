from django.conf import settings
from django.core.management.base import BaseCommand
from django.db.models import Sum
from telegram import Bot
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, Update
from telegram.ext import CallbackContext
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, ConversationHandler
from telegram.utils.request import Request
from bot.models import Profile, Tovar, Message, Dolg
from datetime import datetime, timedelta

FIRST, SECOND, THIRD, WHO, IOP = range(5)
c1, c2, c3, col, fant, sprit, rb, watr, lim, te, cluba, sandw, burgr, dg, don, pani, beno, mar, snik, twx, bunty, cke, back_menu = range(
    23)

ct = {
    '1': "food",
    '2': "drinks",
    '3': "snacks",
}


def split_array(arr, size):
    two_dim_arr = []
    while len(arr) > size:
        part = arr[:size]
        two_dim_arr.append(part)
        arr = arr[size:]
    two_dim_arr.append(arr)
    return two_dim_arr


def log_errors(f):
    def inner(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:

            error_message = f'Произошла ошибка: {e}'
            print(error_message)
            raise e

    return inner


@log_errors
def start(update: Update, context: CallbackContext):
    name = update.effective_user.first_name
    chat_id = update.effective_chat.id
    username = update.message.chat.username
    p, _ = Profile.objects.get_or_create(
        iduser=chat_id,
        defaults={
            'username': username,
            'name': name,
        }
    )
    keyboard = [
        [
            InlineKeyboardButton("food", callback_data=str(c1)),
        ],
        [
            InlineKeyboardButton("drinks", callback_data=str(c2)),
        ],
        [
            InlineKeyboardButton("snacks", callback_data=str(c3)),
        ],
        [
            InlineKeyboardButton("snacks", callback_data=str(c3)),
        ],

    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text(f'{name} Выбери', reply_markup=reply_markup)
    return FIRST


@log_errors
def end(update: Update, context: CallbackContext):
    name = update.effective_user.first_name
    chat_id = update.effective_chat.id
    username = update.message.chat.username
    context.bot.sendMessage(chat_id, "Долг отменен")
    return ConversationHandler.END


@log_errors
def start_back(update: Update, context: CallbackContext):
    name = update.effective_user.first_name
    query = update.callback_query
    query.answer()
    keyboard = [
        [
            InlineKeyboardButton("food", callback_data=str(c1)),
        ],
        [
            InlineKeyboardButton("drinks", callback_data=str(c2)),
        ],
        [
            InlineKeyboardButton("snacks", callback_data=str(c3)),
        ],

    ]
    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(
        text=f'{name} Выбери', reply_markup=reply_markup
    )
    return SECOND


@log_errors
def drink(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    a = Tovar.objects.filter(category='drink').values_list('name', flat=True)
    keyboard_list = []
    for tovar in a:
        keyboard_list.append(InlineKeyboardButton(tovar, callback_data=f'DRINKS---{tovar}'))
    keyboard = split_array(keyboard_list, 3)

    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(
        text="DRINKS", reply_markup=reply_markup
    )
    return SECOND


@log_errors
def food(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    a = Tovar.objects.filter(category='food').values_list('name', flat=True)
    keyboard_list = []
    for tovar in a:
        keyboard_list.append(InlineKeyboardButton(tovar, callback_data=f'FOOD---{tovar}'))
    keyboard = split_array(keyboard_list, 3)

    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(
        text="FOOD", reply_markup=reply_markup
    )
    return SECOND


@log_errors
def snack(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    a = Tovar.objects.filter(category='snack').values_list('name', flat=True)
    keyboard_list = []
    for tovar in a:
        keyboard_list.append(InlineKeyboardButton(tovar, callback_data=f'SNACKS---{tovar}'))
    keyboard = split_array(keyboard_list, 3)

    reply_markup = InlineKeyboardMarkup(keyboard)
    query.edit_message_text(
        text="SNACKS", reply_markup=reply_markup
    )
    return SECOND


@log_errors
def tovar(update: Update, context: CallbackContext):
    query = update.callback_query
    query.answer()
    name = update.effective_user.first_name
    profile = Profile.objects.filter(name=name).first()
    iduser = update.effective_chat.id
    query.edit_message_text(
        text=f'{name}, ok',
    )

    p, _ = Profile.objects.get_or_create(
        iduser=iduser,
        defaults={
            'username': profile.username,
            'name': name,
        }
    )
    t = Tovar.objects.filter(name=query.data.split('---')[1]).first()
    m = Dolg(
        user=p,
        tovar=t
    )
    m.save()
    return ConversationHandler.END


@log_errors
def admin(update: Update, context: CallbackContext):
    iduser = update.effective_chat.id
    user = Profile.objects.filter(iduser=iduser).first()
    if user.is_admin:
        reply_text2 = 'Долги:\n'
        all_users = Profile.objects.all()
        today = datetime.now()
        for one_user in all_users:
            dolgs = Dolg.objects.filter(user=one_user, created_at__month=today.month).values('tovar__name').annotate(
                sum_amount=Sum('amount')
            ).values('tovar__name', 'sum_amount', 'tovar__inprice').distinct()
            if dolgs:
                reply_text2 += f'{one_user.name}:\n'
                for dolg in dolgs:
                    reply_text2 += f'{dolg["tovar__name"]}: {dolg["sum_amount"]} | {dolg["tovar__inprice"]} | {dolg["tovar__inprice"] * dolg["sum_amount"]}\n'

        context.bot.sendMessage(iduser, reply_text2)
    else:
        context.bot.sendMessage(iduser, "Тебе че делать нечего?")


@log_errors
def admin_prev(update: Update, context: CallbackContext):
    iduser = update.effective_chat.id
    user = Profile.objects.filter(iduser=iduser).first()
    if user.is_admin:
        reply_text2 = 'Долги:\n'
        all_users = Profile.objects.all()
        today = datetime.now().replace(day=1)
        yest_month = today - timedelta(days=1)
        for one_user in all_users:
            dolgs = Dolg.objects.filter(user=one_user, created_at__month=yest_month.month).values('tovar__name').annotate(
                sum_amount=Sum('amount')
            ).values('tovar__name', 'sum_amount', 'tovar__inprice').distinct()
            if dolgs:
                reply_text2 += f'{one_user.name}:\n'
                for dolg in dolgs:
                    reply_text2 += f'{dolg["tovar__name"]}: {dolg["sum_amount"]} | {dolg["tovar__inprice"]} | {dolg["tovar__inprice"] * dolg["sum_amount"]}\n'

        context.bot.sendMessage(iduser, reply_text2)
    else:
        context.bot.sendMessage(iduser, "Тебе че делать нечего?")


class Command(BaseCommand):
    start = 'Telegram bot'

    def handle(self, *args, **options):
        request = Request(
            connect_timeout=0.5,
            read_timeout=1.0,

        )
        bot = Bot(
            request=request,
            token=settings.TOKEN

        )
        print(bot.get_me())
        updater = Updater(
            bot=bot,
            use_context=True,

        )
        conv_handler = ConversationHandler(
            entry_points=[CommandHandler('start', start)],
            states={
                FIRST: [
                    CallbackQueryHandler(food, pattern='^' + str(c1) + '$'),
                    CallbackQueryHandler(drink, pattern='^' + str(c2) + '$'),
                    CallbackQueryHandler(snack, pattern='^' + str(c3) + '$'),
                ],
                SECOND: [
                    CallbackQueryHandler(tovar, pattern='^(.+)---(.+)'),
                    CallbackQueryHandler(start_back, pattern='^' + str(back_menu) + '$'),

                ],
            },
            fallbacks=[CommandHandler('end', end)],

        )
        updater.dispatcher.add_handler(CommandHandler('admin', admin))
        updater.dispatcher.add_handler(CommandHandler('admin_prev', admin_prev))
        updater.dispatcher.add_handler(conv_handler)
        updater.start_polling()
        updater.idle()
