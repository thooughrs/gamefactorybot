from django.db import models


class Profile(models.Model):
    iduser = models.PositiveIntegerField(

        verbose_name='ID user tg'
    )
    username = models.CharField(
        max_length=20,
        verbose_name='username',
        db_index=True,
    )
    name = models.CharField(
        max_length=20,
        verbose_name='name',
        db_index=True,
    )
    is_admin = models.BooleanField(
        default=False
    )

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def __str__(self):
        return self.name


class Tovar(models.Model):
    choices = (
        ('food', "food"),
        ('snack', "snack"),
        ('drink', "drink"),

    )
    name = models.CharField(
        max_length=15,
        default=None,
    )
    category = models.CharField(
        max_length=15,
        null=True,
        blank=True,
        choices=choices,
    )
    price = models.IntegerField(
        verbose_name='цена'
    )
    inprice = models.IntegerField(
        verbose_name='Себестоимость',
        null = True,
        blank = True,
    )

    def __str__(self):
        return f'tovar {self.pk}'

    class Meta:
        verbose_name = 'tovars'


class Dolg(models.Model):
    user = models.ForeignKey(
        Profile,
        verbose_name='Отправитель',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default=None

    )
    tovar = models.ForeignKey(
        Tovar,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,

    )
    amount = models.IntegerField(
        verbose_name='копичество',
        default=1,

    )
    created_at = models.DateTimeField(
        verbose_name='time created',
        auto_now_add=True,

    )

    def __str__(self):
        return f'Message {self.pk} от {self.user}'

    class Meta:
        verbose_name = 'dolg'


class Message(models.Model):
    user = models.ForeignKey(
        Profile,
        verbose_name='Отправитель',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        default=None

    )
    created_at = models.DateTimeField(
        verbose_name='time created',
        auto_now_add=True,

    )

